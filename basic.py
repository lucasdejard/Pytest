from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--disable-dev-shm-usage')
options.add_argument('--no-sandbox')

drivern = Remote(options=options, command_executor='http://chrome:4444/wd/hub', desired_capabilities={'browserName':'chrome'})

# driver.get('http://google.com')

# print(driver.page_source)
# driver.quit()

drivern.get("http://testphp.vulnweb.com/login.php")
clicknumbern = drivern.find_element(By.XPATH, '//a[normalize-space()="Contact Us"]')
drivern.quit()
